/* Author: Maximino Suárez van Gelderen
/*I am going to do some procedures to get to know the data set of hotel bookings reservations.
Using the following data set
https://www.kaggle.com/jessemostipak/hotel-booking-demand?select=hotel_bookings.csv*/
/* Archivo de origen: hotel_bookings.csv */
/* Código generado el: 14/5/20 18:53 */

%LET path=/folders/myfolders/sasuser.v94/playingwithdatainsas;
%web_drop_table(WORK.IMPORT2);


FILENAME REFFILE '/folders/myfolders/sasuser.v94/playingwithdatainsas/hotel_bookings.csv';

PROC IMPORT DATAFILE=REFFILE
	DBMS=CSV
	OUT=WORK.RESERVATIONS;
	GETNAMES=YES;
RUN;

%MACRO Content;
	PROC CONTENTS DATA=WORK.RESERVATIONS; RUN;
%mend;


%web_open_table(WORK.RESERVATIONS);

/*We will use proc means to see if any numeric variable is missing, checking it we see there isnt, all numeric variables have 119390 intro*/
%MACRO procmeans;
	ods proclabel="Proc means";
	proc means data=reservations;
	run;
%mend;


	data few_entries/view=few_entries;
	  set reservations nobs=__nobs;
	  if _n_ le 3 or _n_ gt __nobs-3;
	run;
%MACRO fewentries;
 	ods proclabel="Showing some data entries";
	proc print data=few_entries;
	title1 "First 3 and last 3 observations";
	run;
%mend;

/*We can see that some reservations dont have a company nor an agent*/

/*With this set we can analyse what kind of customers come depending on the month, how many days they usually stay, how many children come, when are high-sesion period..
and so on*/


/*First we want to know where most reservations come from*/
%MACRO rankingCountries;
	ods proclabel="Creating ranking by countries";
	title 'Ranking of countries that made more reservations';
	proc freq data=reservations(KEEP=country) order=freq;
	run;
%mend;

/*We will add the column of total nights of stays*/
data reservations;
	set reservations;
	stays_in_total_nights = stays_in_weekend_nights + stays_in_week_nights;
run;

/*We will make a format to make intervals of the days ahead they reserved*/
proc format;
value time_ahead
low-1="day before"
1<-7="less than a week"
7<-30="between a week and a month"
30<-90="between one or three months"
90<-365="between three months and a year"
365<-high="more than a year";
run;

proc format;
value cancel
0="no"
1="yes";
run;

data reservations;
	set reservations;
	format lead_time time_ahead. is_canceled cancel.;
run;


/*When where most reservations done that where after canceled?*/
data cancel_reservations;
	set reservations;
	if is_canceled=1;
run;

%MACRO cancelReservations;
	ods proclabel="When where most resrevations done that where after canceled?";
	title 'When where canceled reservations done';
	proc freq data=cancel_reservations(KEEP=lead_time);
	run;
%mend;


%MACRO daysReservationMonth;
	ods proclabel="Histogram of total days of reservation by month";
	title 'Histogram of total days of reservation';
	proc sgplot data=reservations;
	  histogram stays_in_total_nights;
	run;
%mend;


proc sort data=reservations(KEEP=arrival_date_month stays_in_total_nights) out=stays_month;
	by arrival_date_month;
run;

%MACRO meanNightResrevationMonth;
	ods proclabel="Mean of reservations nights each month";
	title 'Mean of reservations nights each month';
	proc sgplot data=stays_month;
	  vbar arrival_date_month / response=stays_in_total_nights stat=mean;
	run;
%mend;



/*I dont see any two variables easily related to do an scatter
proc sgplot data=reservations;
   scatter x= y=;
run;
*/

ODS HTML PATH="&path" (url=none) BODY='body.html' CONTENTS='output.html';
/*(url=none)  - if you want that link works in other localizations*/

/*Everything we want to have in report:*/
%content;
%procmeans;
%fewentries;
%rankingCountries;
%cancelReservations;
%daysReservationMonth;
%meanNightResrevationMonth;

ODS HTML CLOSE;











	






