# PlayingWithDataInSAS

I am going to do some procedures to get to know the data set of countries using the program SAS.

### Thought about the data set:

With this set we can identify caracteristics about the hotel reservations in this area.
We can get where people come from, when people reserve more, if people that reserve with
a lot of time ahead cancel more or not, how many days they usually stay.. etc.

To get this information we can use tables, graphs, sort the data, drop a columns that we dont need to clarify the results..

We will need proc sort, proc means proc contents..


### Conclusions:

Most reservations come from Portugal with a 40%, after it Great Britain France and Spain 
have the lead with a 10% approximately each.

Most canceletion where from reservation that where made between 3 and a year before (53% of all cancelations)

Most people reserve from 1 to 5 nights. Each month, the average of nigh reserve per reservation is between 3 and 4. 
Being july and august the months with highest average and january the lowest.



